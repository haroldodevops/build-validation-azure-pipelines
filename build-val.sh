#!/bin/bash

ORGANIZATION="https://dev.azure.com/ORGANIZATION"
OUTPUT_FILE="projetos.json"
REPOS_OUTPUT_FILE="repos.json"
BUILD_DEFS_OUTPUT_FILE="build_defs.json"

# Autentica com o Azure usando o comando 'az login'
az login -o table


# Lista os projetos da organização e salva em um arquivo JSON
echo "Iniciando listagem de projetos..."
az devops project list --organization $ORGANIZATION --output json | tee "$OUTPUT_FILE"
echo "Listagem de projetos concluída e salva no arquivo '$OUTPUT_FILE'."

# Imprime um menu com o nome de cada projeto
echo "Escolha um projeto para listar:"
jq -r '.value[] | "\(.name)"' "$OUTPUT_FILE" | awk '{ print NR, $0 }'

# Pede ao usuário para escolher um projeto
read -p "Digite o número do projeto desejado: " project_number

# Obtém o id do projeto selecionado
project_id=$(jq -r ".value[$project_number - 1].id" "$OUTPUT_FILE")

# Lista as definições de build do projeto selecionado e salva em um arquivo JSON
echo "Iniciando listagem de definições de build..."
az pipelines build definition list --organization $ORGANIZATION --project $project_id --output json | tee "$BUILD_DEFS_OUTPUT_FILE"
echo "Listagem de definições de build concluída e salva no arquivo '$BUILD_DEFS_OUTPUT_FILE'."

# Imprime um menu com o nome de cada definição de build
echo "Escolha uma definição de build para aplicar a política:"
jq -r '.[] | "\(.name)"' "$BUILD_DEFS_OUTPUT_FILE" | awk '{ print NR, $0 }'

# Pede ao usuário para escolher uma definição de build
read -p "Digite o número da definição de build desejada: " build_def_number

# Obtém o id da definição de build selecionada
build_def_id=$(jq -r ".[$build_def_number - 1].id" "$BUILD_DEFS_OUTPUT_FILE")

# Lista os repositórios do projeto selecionado e salva em um arquivo JSON
echo "Iniciando listagem de repositórios..."
az repos list --organization $ORGANIZATION --project $project_id --output json | tee "$REPOS_OUTPUT_FILE"
echo "Listagem de repositórios concluída e salva no arquivo '$REPOS_OUTPUT_FILE'."

# Imprime um menu com o nome de cada repositório
echo "Escolha um repositório para aplicar a política:"
select repo_name in $(jq -r '.[] | .name' "$REPOS_OUTPUT_FILE"); do
    if [ ! -z "$repo_name" ]; then
        repo_id=$(jq -r ".[] | select(.name == \"$repo_name\") | .id" "$REPOS_OUTPUT_FILE")
        echo "Aplicando política de validação de build no repositório '$repo_name'..."
        az repos policy build create \
            --blocking true \
            --branch "main" \
            --build-definition-id "$build_def_id" \
            --repository-id "$repo_id" \
            --enabled true \
            --queue-on-source-update-only false \
            --manual-queue-only true \
            --display-name "Manual queue policy" \
            --valid-duration 0 
        break
    fi
done

