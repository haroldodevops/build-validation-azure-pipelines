# Implementation of build validation policies in Azure Pipelines

# README

This bash script was created to automate the configuration of a build validation policy in an Azure DevOps repository. The goal is to streamline the process of applying the policy to a specific project.

## Requirements

Before running the script, make sure you have the Azure CLI (`az`) installed and are logged in to your Azure account.

## Configuration

Before running the script, you need to adjust some variables according to your environment:

- `ORGANIZATION`: URL of the Azure DevOps organization.
- `OUTPUT_FILE`: Name of the JSON file where the project list will be stored.
- `REPOS_OUTPUT_FILE`: Name of the JSON file where the repository list will be stored.
- `BUILD_DEFS_OUTPUT_FILE`: Name of the JSON file where the build definition list will be stored.

## Execution

To run the script, open a terminal and navigate to the directory where the script is located. Then, execute the following command:

```
./script-name.sh
```

The script will perform the following actions:

1. Authenticate with Azure using the `az login` command.
2. List the projects in the organization and save them to a JSON file.
3. Display a menu with the name of each project and prompt the user to choose a project.
4. Get the ID of the selected project.
5. List the build definitions for the selected project and save them to a JSON file.
6. Display a menu with the name of each build definition and prompt the user to choose one.
7. Get the ID of the selected build definition.
8. List the repositories for the selected project and save them to a JSON file.
9. Display a menu with the name of each repository and prompt the user to choose one.
10. Apply the build validation policy to the selected repository.

During the script execution, progress messages and user input prompts will be displayed.

Make sure to properly configure the build validation policy options in the script section where the `az repos policy build create` command is invoked.

After successful execution of the script, the build validation policy will be applied to the chosen repository.

---
**Note**: Ensure that you have the proper permissions in the Azure DevOps organization to perform the necessary actions. Otherwise, the script may not function correctly.


## Linguagem PT-BR
# README

Este script em bash foi criado para automatizar a configuração de uma política de validação de build em um repositório do Azure DevOps. O objetivo é facilitar o processo de aplicação da política em um projeto específico.

## Requisitos

Antes de executar o script, é necessário ter instalado o Azure CLI (`az`) e estar autenticado na sua conta do Azure.

## Configuração

Antes de executar o script, você precisa ajustar algumas variáveis de acordo com o seu ambiente:

- `ORGANIZATION`: URL da organização do Azure DevOps.
- `OUTPUT_FILE`: Nome do arquivo JSON onde a lista de projetos será armazenada.
- `REPOS_OUTPUT_FILE`: Nome do arquivo JSON onde a lista de repositórios será armazenada.
- `BUILD_DEFS_OUTPUT_FILE`: Nome do arquivo JSON onde a lista de definições de build será armazenada.

## Execução

Para executar o script, abra um terminal e navegue até o diretório onde o script está localizado. Em seguida, execute o seguinte comando:

```
./nome-do-script.sh
```

O script irá realizar as seguintes ações:

1. Autenticar-se no Azure usando o comando `az login`.
2. Listar os projetos da organização e salvar em um arquivo JSON.
3. Exibir um menu com o nome de cada projeto e solicitar ao usuário que escolha um projeto.
4. Obter o ID do projeto selecionado.
5. Listar as definições de build do projeto selecionado e salvar em um arquivo JSON.
6. Exibir um menu com o nome de cada definição de build e solicitar ao usuário que escolha uma.
7. Obter o ID da definição de build selecionada.
8. Listar os repositórios do projeto selecionado e salvar em um arquivo JSON.
9. Exibir um menu com o nome de cada repositório e solicitar ao usuário que escolha um.
10. Aplicar a política de validação de build no repositório selecionado.

Durante a execução do script, serão exibidas mensagens de progresso e solicitações de entrada do usuário.

Lembre-se de configurar corretamente as opções da política de validação de build no trecho do script onde é invocado o comando `az repos policy build create`.

Após a execução bem-sucedida do script, a política de validação de build será aplicada ao repositório escolhido.

---
**Observação**: Certifique-se de ter as permissões adequadas na organização do Azure DevOps para executar as ações necessárias. Caso contrário, o script pode não funcionar corretamente.